use std::fs::File;
use std::fs::OpenOptions;
use std::io::{self, Write};
use std::path::Path;
use std::process::Command;
use chrono::prelude::*;

fn write_to_log(log: &str) -> io::Result<()> {
    let utc: DateTime<Utc> = Utc::now();

    if Path::new("lisa.log").exists() {
        let mut file = OpenOptions::new().append(true).open("lisa.log")?;
        writeln!(file, "--------------{}--------------", utc)?;
        writeln!(file, "{}", log)?;
    } else {
        let mut file = File::create("lisa.log")?;
        writeln!(file, "--------------{}--------------", utc)?;
        writeln!(file, "{}", log)?;
    }
    Ok(())
}

fn is_installed(command: &str) -> bool {
    let output = Command::new("which")
        .arg(command)
        .output()
        .expect("failed to execute process");

    output.status.success()
}

fn check_which_distro() -> Option<String> {
    let pkm_to_check: [&str; 3] = ["apt", "pacman", "dnf"];
    for pkm in pkm_to_check.iter() {
        if is_installed(pkm) {
            return Some(pkm.to_string());
        }
    }
    None
}

fn _speed_up_dnf() {
    /* finish later */
    /*
        # see `man dnf.conf` for defaults and possible options

    [main]
    gpgcheck=True
    installonly_limit=3
    clean_requirements_on_remove=True
    best=False
    skip_if_unavailable=True
    max_parallel_downloads=10
    fastestmirror=True
    defaultyes=True
         */
}

fn run_dnf_update() {
    let output = Command::new("sudo")
        .arg("dnf")
        .arg("update")
        .arg("-y")
        .output()
        .expect("failed to execute process");

    match output.status.success() {
        true => {
            let log = String::from_utf8_lossy(&output.stdout);
            write_to_log(&log).expect("failed to write to log");
        }
        false => {
            let log = String::from_utf8_lossy(&output.stderr);
            write_to_log(&log).expect("failed to write to log");
        }
    }
}

fn run_fedora_stuff() {
    // speed_up_dnf()
    run_dnf_update();
    println!("Successfully updated Fedora!");
}

fn run_fn_to_distro() {
    let pkm = check_which_distro();
    if let Some(pkm) = pkm {
        if pkm == "dnf" {
            run_fedora_stuff();
        }
    }
}

fn main() {
    run_fn_to_distro();
}
